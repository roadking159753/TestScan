resource "aws_s3_bucket" "unrestricted9" {
  bucket = "examplebuckettftest1"
  #zpc-skip-policy: ZS-AWS-00026:testing
  acl    = "public-read-write"
  versioning {
    enabled = true
  }
  logging {
    target_bucket = aws_s3_bucket.log_bucket.id
    target_prefix = "log/"
    }
}

resource "aws_s3_bucket" "unrestricted8" {
  bucket = "examplebuckettftest"
  #zpc-skip-policy: ZS-AWS-00026:testing
  acl    = "public-read-write"
  versioning {
    enabled = true
  }
  logging {
    target_bucket = aws_s3_bucket.log_bucket.id
    target_prefix = "log/"
    }
}

resource "aws_s3_bucket" "unrestricted7" {
  bucket = "examplebuckettftest"
  #zpc-skip-policy: ZS-AWS-00026:testing
  acl    = "public-read-write"
  versioning {
    enabled = true
  }
  logging {
    target_bucket = aws_s3_bucket.log_bucket.id
    target_prefix = "log/"
    }
}
